docker.clean:
	docker rm pytalk
	docker rmi srayner/pytalk

docker.build:
	docker build \
		--platform=linux/amd64 \
		-t srayner/pytalk .

docker.run:
	docker run \
		--platform linux/amd64 \
		--name pytalk \
		srayner/pytalk:latest