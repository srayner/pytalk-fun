#!/usr/bin/env bash
# This is a dev script to help developser run the same formatting locally.
#
# Author: Stephan Rayner

set -euo pipefail
shopt -s inherit_errexit

TARGET_DIR="$(pwd)"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

main() {
	cd ..
	blue ./
	isort --atomic --profile black .
	exit 0
}

pushd "$SCRIPT_DIR" > /dev/null || exit
main $@
popd > /dev/null || exit