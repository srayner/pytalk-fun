import base64
import hashlib
from typing import Any


def md5_hash(data: Any) -> str:
    binary_data: bytes = bytes(data, 'ascii')
    hash: bytes = hashlib.md5(binary_data)
    return hash.hexdigest()


def base64_encode(data: Any) -> str:
    binary_data = bytes(data, 'ascii')
    encoded = base64.b64encode(binary_data)
    return encoded.decode('utf-8')
