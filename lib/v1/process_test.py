import unittest

from . import process


class TestProcess(unittest.TestCase):
    def test_base64_encode_cheese(self):
        expected: str = 'Y2hlZXNl'
        actual: str = process.base64_encode('cheese')
        self.assertEqual(actual, expected)

    def test_base64_encode_empty(self):
        expected: str = ''
        actual: str = process.base64_encode('')
        self.assertEqual(actual, expected)

    def test_md5_cheese(self):
        expected: str = 'fea0f1f6fede90bd0a925b4194deac11'
        actual: str = process.md5_hash('cheese')
        self.assertEqual(actual, expected)

    def test_md5_empty(self):
        expected: str = 'd41d8cd98f00b204e9800998ecf8427e'
        actual: str = process.md5_hash('')
        self.assertEqual(actual, expected)
