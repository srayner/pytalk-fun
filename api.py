"""This is a simple API micorservice to wrap the baseball height predictor model.
"""

from flask import Flask, jsonify, make_response, request

from lib.v1 import process as v1_process

app = Flask(__name__)


@app.errorhandler(404)
def not_found(e):
    return make_response(
        jsonify({'message': 'The endpoint you specified was not found.'}), 404
    )


@app.errorhandler(500)
def internal_server_error(e):
    return make_response(jsonify({'error': str(e)}), 500)


@app.route('/')
def index():
    payload = {'author': 'Stephan Rayner', 'endpoints': ['/', '/v1/compute']}
    return make_response(jsonify(payload), 200)


@app.route('/v1/compute', methods=['GET'])
def compute():
    input_value = request.args.get('value', default=None, type=str)

    if input_value is None:
        return make_response(
            jsonify(
                {
                    'message': 'Bad Request: `value` is a required parameter for this endpoint and it should be a number'
                }
            ),
            400,
        )

    payload: dict = {
        'base64': v1_process.base64_encode(input_value),
        'md5': v1_process.md5_hash(input_value),
    }
    response_body = {'value': input_value, 'payload': payload}
    res = make_response(jsonify(response_body), 200)
    return res


if __name__ == '__main__':
    app.run(port=5000, debug=True)
