# PyTalk Fun

This is a repo to house my CI/CD with Python on GitLab Talk. Hopefully one day it will house more talks than that! :)

## Resources

- https://docs.gitlab.com/ee/topics/autodevops/
- https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#python-pip
- https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/lib/gitlab/ci/templates/Jobs
- https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/lib/gitlab/ci/templates/Security
- https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/AWS/Deploy-ECS.gitlab-ci.yml
- https://docs.gitlab.com/ee/ci/cloud_deployment/ecs/deploy_to_aws_ecs.html
- https://docs.gitlab.com/ee/ci/caching/#cache