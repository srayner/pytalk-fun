FROM python:3.10-slim

WORKDIR /pytalk

ADD . .

RUN pip install --upgrade pip;\
    pip install -r ./requirements.txt;

CMD gunicorn --config ./gunicorn.conf.py api:app